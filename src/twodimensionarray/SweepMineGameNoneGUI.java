package twodimensionarray;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class SweepMineGameNoneGUI {
	int theNumberOfMine;
	int[][] playGround;
	ArrayList<String> listOfInputCoordinate = new ArrayList<>();

	public SweepMineGameNoneGUI(int sizeOfPlayGround, int theNumberOfMine) {
		// TODO Auto-generated constructor stub
		if (theNumberOfMine < ((sizeOfPlayGround * sizeOfPlayGround) * 2 / 3)) {
			this.theNumberOfMine = theNumberOfMine;
			this.playGround = createPlayGround(sizeOfPlayGround, theNumberOfMine);
		} else {
			System.out.println("Too many mine");
		}

	}

	private int[][] createPlayGround(int size, int numberMime) {
		return countMine(putMine(size, numberMime));
	}

	private int[][] putMine(int sizeOfPlayGround, int numberOfMineP) {
		// create a array to put mine on this
		int tempX, tempY, numberOfMine = numberOfMineP;
		ArrayList<String> listOfMine = new ArrayList<String>();
		String tempCordinateOfMine;

		// We will create a array which has two more element than playground. It
		// make counting mine method will be easier than the array which fix
		// size with play ground.
		int[][] result = new int[sizeOfPlayGround + 2][sizeOfPlayGround + 2];

		// create random object
		Random random = new Random();

		while (numberOfMine > 0) {
			tempX = random.nextInt(sizeOfPlayGround) + 1;
			tempY = random.nextInt(sizeOfPlayGround) + 1;
			tempCordinateOfMine = "" + tempX + tempY;
			if (!listOfMine.contains(tempCordinateOfMine)) {
				listOfMine.add(tempCordinateOfMine);
				result[tempX][tempY] = -1;
				numberOfMine--;
			}
		}
		return result;
	}

	private int[][] countMine(int[][] mineArray) {
		// check all element of play ground array
		// We start at coordinate (1; 1) and end at (size -1) because we have
		// created a array has two more element than playground. So that we
		// ignore first and last row, column.
		for (int i = 1; i < mineArray.length - 1; i++) {
			for (int j = 1; j < mineArray[i].length - 1; j++) {
				// if one of element is not mine it will have the number of mine
				// around it.
				if (mineArray[i][j] != -1) {
					int count = 0;
					// (i-1; j-1) (i-1; j) (i-1; j+1)
					// (i; j-1) (i; j) (i; j+1)
					// (i+1, j-1) (i+1; j) (i+1; j+1)
					for (int i2 = i - 1; i2 <= i + 1; i2++) {
						for (int j2 = j - 1; j2 <= j + 1; j2++) {
							if (mineArray[i2][j2] == -1) {
								count++;
							}
						}
					}
					mineArray[i][j] = count;
				}
			}
		}
		return mineArray;
	}

	public boolean playGame(int xInput, int yInput) {
		int realSizeOfPlayGround = this.playGround.length - 2;
		// check if the number of remain element equal the number of mine, the player will win this game.
		if (realSizeOfPlayGround * realSizeOfPlayGround - this.listOfInputCoordinate.size() == this.theNumberOfMine) {
			System.out.println("You win");
			return false;
		}
		String tempCoordinate = "" + xInput + yInput;
		if (!this.listOfInputCoordinate.contains(tempCoordinate)) {
			this.listOfInputCoordinate.add(tempCoordinate);
		}
		for (int i = 1; i < this.playGround.length - 1; i++) {
			for (int j = 1; j < this.playGround[i].length - 1; j++) {
				// if that element is not mine show the number of mine around it
				String checkingCoordinate = "" + i + j;

				// if that element was shown before. it will be show again until
				// the end.
				if (this.listOfInputCoordinate.contains(checkingCoordinate) && this.playGround[i][j] != -1) {
					System.out.print(this.playGround[i][j] + " ");
				} else if (this.listOfInputCoordinate.contains(checkingCoordinate) && this.playGround[i][j] == -1) {
					System.out.println("Here is mine. You lost.");
					for (int k = 1; k < playGround.length - 1; k++) {
						for (int k2 = 1; k2 < playGround[k].length - 1; k2++) {
							System.out.print(this.playGround[k][k2] + "\t");
						}
						System.out.println();
					}
					return false;
				} else {
					System.out.print("x" + " ");
				}
			}
			System.out.println();
		}
		System.out.println("----------------------------");
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.print("How large of play ground do you want to play: ");
		int sizeOfPlayGround = scanner.nextInt();
		System.out.print("How namy mine do you want to put on play ground: ");
		int numberOfMine = scanner.nextInt();
		SweepMineGameNoneGUI play = new SweepMineGameNoneGUI(sizeOfPlayGround, numberOfMine);
		System.out.println("Now, we have a play ground.");
		while (true) {
			System.out.println("Put coordinate to sweep mine: ");
			System.out.print("x: ");
			int x = scanner.nextInt();
			System.out.print("y: ");
			int y = scanner.nextInt();
			if (!play.playGame(x, y)) {
				break;
			}
		}
	}

}
