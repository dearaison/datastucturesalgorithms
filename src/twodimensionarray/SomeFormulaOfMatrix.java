package twodimensionarray;

public class SomeFormulaOfMatrix {
	private static boolean checkIsSquareMatrix(int[][] array) {
		for (int i = 0; i < array.length - 1; i++) {
			if (array[i].length != array[i + i].length) {
				return false;
			}
		}
		return true;
	}

	private static String toString(int[][] array) {
		// TODO Auto-generated method stub
		String result = "";
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				result += array[i][j] + " ";
			}
			result += "\n";
		}
		return result;
	}

	public int[][] multiplyANumberAndAMatrix(int[][] array, int number) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = array[i][j] * number;
			}
		}
		return array;
	}

	public int[][] addTwoMatrix(int[][] array1, int[][] array2) {
		int[][] result = new int[array1.length][array1[0].length];
		if (checkIsSquareMatrix(array2) && checkIsSquareMatrix(array2) && array1.length == array2.length
				&& array1[0].length == array2[0].length) {
			for (int i = 0; i < result.length; i++) {
				for (int j = 0; j < result[i].length; j++) {
					result[i][j] = array1[i][j] + array2[i][j];
				}
			}
		}
		return result;
	}

	public int[][] multiplyTwoMatrix(int[][] array1, int[][] array2) {
		int[][] result = new int[array1.length][array2[0].length];
		if (array1.length == array2[0].length) {
			for (int i = 0; i < result.length; i++) {
				for (int j = 0; j < result[i].length; j++) {
					for (int j2 = 0; j2 < array2.length; j2++) {
						result[i][j] += array1[i][j2] * array2[j2][j];
					}
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] array = { { 1, 2, 3, 4 }, { 5, 8, 5, 5 }, { 8, 9, 5, 4 } };
		int[][] array1 = { { 1, 2, 3, 4 }, { 5, 8, 5, 5 }, { 8, 9, 5, 4 } };
		int[][] array2 = { { 1, 2, 3, 4 }, { 5, 8, 5, 5 }, { 8, 9, 5, 4 } };
		int[][] arr1 = { { 5, 7, 9, 3 }, { 2, 6, 8, 1 } };
		int[][] arr2 = { { 2, 7 }, { 5, 6 }, { 9, 10 }, { 2, 4 } };
		SomeFormulaOfMatrix test1 = new SomeFormulaOfMatrix();
		System.out.println(toString(test1.multiplyANumberAndAMatrix(array, 2)));
		System.out.println();
		System.out.println(toString(test1.addTwoMatrix(array1, array2)));
		System.out.println();
		System.out.println(toString(test1.multiplyTwoMatrix(arr1, arr2)));

	}

}
