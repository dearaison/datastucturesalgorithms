package setexercise;

import java.util.ArrayList;
import java.util.TreeSet;

public class Bill {
	String customerName;
	String creatingDate;
	TreeSet<Product> cart;

	public Bill(String customerName, String creatingDate) {
		super();
		this.customerName = customerName;
		this.creatingDate = creatingDate;
		this.cart = new TreeSet<Product>();
	}

	public void add(Product newProduct) {
		this.cart.add(newProduct);
	}

	public void remove(Product re) {
		this.cart.remove(re);
	}

	public Product find(String name) {
		if (this.cart.isEmpty())
			return null;
		for (Product a : this.cart) {
			if (a.productName.equalsIgnoreCase(name)) {
				return a;
			}
		}
		return null;
	}

	public Product findByBeginOfChar(String chara) {
		for (Product a : this.cart) {
			if (a.productName.startsWith(chara)) {
				return a;
			}
		}
		return null;
	}

	public ArrayList<Product> findLargerThan(int limitNum) {
		ArrayList<Product> res = new ArrayList<Product>();
		for (Product matHang : this.cart) {
			if (matHang.theNumber > limitNum) {
				res.add(matHang);
			}
		}
		return res;
	}

	public void addSpecial(Product that) {
		for (Product matHang : cart) {
			if (matHang.productName.equalsIgnoreCase(that.productName)) {
				int newNumber = matHang.getTheNumber() + that.getTheNumber();
				matHang.setTheNumber(newNumber);
			}
		}
		this.cart.add(that);
	}

	public boolean removeSpecial(Product that) {
		int newNumber;
		for (Product matHang : cart) {
			if (matHang.productName.equalsIgnoreCase(that.productName)) {
				if (matHang.getTheNumber() < that.getTheNumber()) {
					System.out.println("out of the number of product in cart");
					return false;
				}
				if (matHang.getTheNumber() == that.getTheNumber()) {
					this.cart.remove(matHang);
					return true;
				}
				if (matHang.getTheNumber() > that.getTheNumber()) {
					newNumber = matHang.getTheNumber() - that.getTheNumber();
					matHang.setTheNumber(newNumber);
					return true;

				}
			}
		}
		if (this.cart.contains(that)) {
			this.cart.remove(that);
			return true;
		}
		return false;
	}

	public double computeTotalPrice() {
		double res = 0;
		for (Product product : cart) {
			res += product.price * product.theNumber;
		}
		return res;
	}

	public void printBill() {
		System.out
				.println("customerName: " + customerName + ", creatingDate: " + creatingDate + "\n" + "cart:\n" + cart);
		System.out.println("Total: " + this.computeTotalPrice());
	}

	@Override
	public String toString() {
		return "customerName: " + customerName + ", creatingDate: " + creatingDate + "\n" + "cart:\n" + cart;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Product ct1 = new Product("Mi1", "Mi goi Gau Do", 3500, 12);
		Product ct2 = new Product("Mi2", "Mi goi Vi Huong", 3000, 10);
		Product ct3 = new Product("Mi3", "Mi goi Udon", 7500, 8);
		Product ct4 = new Product("Ga1", "Ga Ham Vissan", 23500, 3);
		Product ct5 = new Product("Kem1", "Kem Socola Wall's", 13500, 4);
		Product ct6 = new Product("Mi1", "Mi goi Gau Do", 3500, 4);
		Product ct7 = new Product("Mi1", "Mi goi Gau Do", 3500, 13);
		Bill dh = new Bill("Q001", "23/11/2016");
		dh.add(ct1);
		dh.add(ct2);
		dh.add(ct3);
		dh.add(ct4);
		dh.add(ct5);
		dh.addSpecial(ct6);
		dh.removeSpecial(ct7);

		// System.out.println(dh);

		dh.printBill();
	}

}