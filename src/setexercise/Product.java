package setexercise;

public class Product implements Comparable<Product> {
	String productCode;
	String productName;
	double price;
	int theNumber;

	public Product(String maMH, String tenMH, double donGia, int soLuong) {
		super();
		this.productCode = maMH;
		this.productName = tenMH;
		this.price = donGia;
		this.theNumber = soLuong;
	}

	@Override
	public int compareTo(Product that) {
		return this.productCode.compareTo(that.productCode);
	}

	public int getTheNumber() {
		return theNumber;
	}

	public void setTheNumber(int theNumber) {
		this.theNumber = theNumber;
	}

	@Override
	public String toString() {
		return "productCode: " + productCode + ", productName: " + productName
				+ ", price: " + price + ", theNumber: " + theNumber + "\n";
	}

}