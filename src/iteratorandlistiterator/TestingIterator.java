package iteratorandlistiterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

public class TestingIterator {
	ListIterator<Integer> list;

	public TestingIterator(ArrayList<Integer> arrl) {
		super();
		this.list = arrl.listIterator();
	}

	public TestingIterator(Integer[] arr) {
		super();
		this.list = Arrays.asList(arr).listIterator();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
