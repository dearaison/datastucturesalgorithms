package ceasarencryption;

public class ASCIIWay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ASCIIWay test = new ASCIIWay();

		String testString = "a-c*d/@C#D%&3*9(8)7_=abcABCyzYZ6789123";
		System.out.println(testString + "\n");

		String afterEncode = test.encryptAString(testString, 3, true);
		System.out.println(afterEncode + "\n");

		String afterDecode = test.encryptAString(afterEncode, 3, false);
		System.out.println(afterDecode);
	}

	public String encryptAString(String originalString, int rotation, boolean encryption) {
		String res = "";
		int theNumberInASCIIOfCharacter;
		final int THE_NUMBER_IN_ASCII_OF_0 = '0';
		final int THE_NUMBER_IN_ASCII_OF_9 = '9';
		final int THE_NUMBER_IN_ASCII_OF_A = 'A';
		final int THE_NUMBER_IN_ASCII_OF_Z = 'Z';
		final int THE_NUMBER_IN_ASCII_OF_a = 'a';
		final int THE_NUMBER_IN_ASCII_OF_z = 'z';

		if (encryption) {
			for (int index = 0; index < originalString.length(); index++) {
				// get char in String one by one.
				char tempChar = originalString.charAt(index);
				// get the number of char in string in ASCII table.
				theNumberInASCIIOfCharacter = tempChar;
				char swapChar;
				// check tempChar is between 0 and 9 or A and Z or a and z?
				if (THE_NUMBER_IN_ASCII_OF_0 <= theNumberInASCIIOfCharacter
						&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_9
						|| THE_NUMBER_IN_ASCII_OF_A <= theNumberInASCIIOfCharacter
								&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_Z
						|| THE_NUMBER_IN_ASCII_OF_a <= theNumberInASCIIOfCharacter
								&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_z) {
					// if a letter can jump to next rotation because getting out
					// of
					// letter table, we will handle it with this way: get the
					// length
					// of this letter array (in this case we use ASCII table so
					// that
					// the length is the number of the last letter in its array
					// add
					// one, example: THE_NUMBER_InASCII_OF_9 + 1) subtract for
					// index position of this letter in table. If it lower or
					// equal than rotation, it cann't
					// jump to next
					// rotation. So we must get the first letter index in array
					// add
					// with (rotation - (length - the index of this letter in
					// encode
					// array)).
					/*
					 * Example: encode array: 0 1 2 3 4 5 6 7 8 9; length: 10;
					 * index: 8; rotation: 3; We cann't jump because 8 + 3 = 11;
					 * index formula = 0 + (3 - (10 - 8)) = 1; the char after
					 * encrypt is 1;
					 */
					if (THE_NUMBER_IN_ASCII_OF_0 <= theNumberInASCIIOfCharacter
							&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_9
							&& THE_NUMBER_IN_ASCII_OF_9 + 1 - theNumberInASCIIOfCharacter <= rotation) {
						swapChar = (char) (THE_NUMBER_IN_ASCII_OF_0
								+ (rotation - (THE_NUMBER_IN_ASCII_OF_9 + 1 - theNumberInASCIIOfCharacter)));
						tempChar = swapChar;
					} else {
						if (THE_NUMBER_IN_ASCII_OF_a <= theNumberInASCIIOfCharacter
								&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_z
								&& THE_NUMBER_IN_ASCII_OF_z + 1 - theNumberInASCIIOfCharacter <= rotation) {
							swapChar = (char) (THE_NUMBER_IN_ASCII_OF_a
									+ (rotation - (THE_NUMBER_IN_ASCII_OF_z + 1 - theNumberInASCIIOfCharacter)));
							tempChar = swapChar;
						} else {
							if (THE_NUMBER_IN_ASCII_OF_A <= theNumberInASCIIOfCharacter
									&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_Z
									&& THE_NUMBER_IN_ASCII_OF_Z + 1 - theNumberInASCIIOfCharacter <= rotation) {
								swapChar = (char) (THE_NUMBER_IN_ASCII_OF_A
										+ (rotation - (THE_NUMBER_IN_ASCII_OF_Z + 1 - theNumberInASCIIOfCharacter)));
								tempChar = swapChar;
							} else {
								swapChar = (char) (theNumberInASCIIOfCharacter + rotation);
								tempChar = swapChar;
							}
						}
					}
				}
				res += tempChar;
			}

		} else if (!encryption) {
			for (int index = 0; index < originalString.length(); index++) {
				// get char in String one by one.
				char tempChar = originalString.charAt(index);
				// get the number of char in string in ASCII table.
				theNumberInASCIIOfCharacter = tempChar;
				char swapChar;
				// check tempChar is between 0 and 9 or A and Z or a and z?
				if (THE_NUMBER_IN_ASCII_OF_0 <= theNumberInASCIIOfCharacter
						&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_9
						|| THE_NUMBER_IN_ASCII_OF_A <= theNumberInASCIIOfCharacter
								&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_Z
						|| THE_NUMBER_IN_ASCII_OF_a <= theNumberInASCIIOfCharacter
								&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_z) {
					// if a letter can jump down before rotation because getting
					// out of
					// letter table, we will handle it with this way: use -1
					// subtract for
					// index position of this letter in table. If it higher or
					// equal than ((-1) * rotation), it cann't
					// jump down before
					// rotation. So we must get the last letter index in array
					// subtract
					// for (rotation + (-1 - the index of this letter in
					// encode
					// array)). In this case -1 is the first letter in table
					// subtract for 1. Example: THE_NUMBER_IN_ASCII_OF_0 - 1
					/*
					 * Example: encode array: 0 1 2 3 4 5 6 7 8 9; length: 10;
					 * index: 1; rotation: 3; We cann't jump because -1 - 1 = -2
					 * > -3 and 1 - 3 = -2; index formula = 9 - (3 + (-1 - 1)) =
					 * 8; the char after encrypt is 1;
					 */
					if (THE_NUMBER_IN_ASCII_OF_0 <= theNumberInASCIIOfCharacter
							&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_9
							&& (THE_NUMBER_IN_ASCII_OF_0 - 1) - theNumberInASCIIOfCharacter >= -rotation) {
						swapChar = (char) (THE_NUMBER_IN_ASCII_OF_9
								- (rotation + ((THE_NUMBER_IN_ASCII_OF_0 - 1) - theNumberInASCIIOfCharacter)));
						tempChar = swapChar;
					} else {
						if (THE_NUMBER_IN_ASCII_OF_a <= theNumberInASCIIOfCharacter
								&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_z
								&& (THE_NUMBER_IN_ASCII_OF_a - 1) - theNumberInASCIIOfCharacter >= -rotation) {
							swapChar = (char) (THE_NUMBER_IN_ASCII_OF_z
									- (rotation + ((THE_NUMBER_IN_ASCII_OF_a - 1) - theNumberInASCIIOfCharacter)));
							tempChar = swapChar;
						} else {
							if (THE_NUMBER_IN_ASCII_OF_A <= theNumberInASCIIOfCharacter
									&& theNumberInASCIIOfCharacter <= THE_NUMBER_IN_ASCII_OF_Z
									&& (THE_NUMBER_IN_ASCII_OF_A - 1) - theNumberInASCIIOfCharacter >= -rotation) {
								swapChar = (char) (THE_NUMBER_IN_ASCII_OF_Z
										- (rotation + ((THE_NUMBER_IN_ASCII_OF_A - 1) - theNumberInASCIIOfCharacter)));
								tempChar = swapChar;
							} else {
								swapChar = (char) (theNumberInASCIIOfCharacter - rotation);
								tempChar = swapChar;
							}
						}
					}
				}
				res += tempChar;
			}
		}

		return res;
	}
}
