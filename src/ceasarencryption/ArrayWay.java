package ceasarencryption;

public class ArrayWay {

	public static String encryptAString(String origin, int key, boolean encryption) {
		String res = "";
		char[] alphabel = new char[62];
		char[] encryptionArray = new char[62];
		for (int i = 0; i < 62; i++) {
			if (i < 26) {
				alphabel[i] = (char) ('A' + i);
				encryptionArray[i] = (char) ('A' + (i + key) % 26);
			} else if (i < 52) {
				/*
				 * 'a' + i - 26 because i now is 26.
				 * 
				 * Example: a + 26 -26 = a.
				 * 
				 * a + 27 - 26 = b.
				 */
				int swapI = i - 26;
				alphabel[i] = (char) ('a' + swapI);
				encryptionArray[i] = (char) ('a' + (swapI + key) % 26);
			} else {
				int swapI = i - 52;
				alphabel[i] = (char) ('0' + swapI);
				encryptionArray[i] = (char) ('0' + (swapI + key) % 10);
				/*
				 * Example: 0 1 2 3 4 5 6 7 8 9 (key = 3)
				 * 
				 * 
				 * case 1: 8 + 3 = 11 => 0 + (8 + 3) % 10 = 1
				 * 
				 * 
				 * case 2: 5 + 3 = 8 => 0 + (5 + 3) % 10 = 8
				 */
			}
		}
		if (encryption) {
			for (int i = 0; i < origin.length(); i++) {
				char tempChar = origin.charAt(i);
				for (int j = 0; j < alphabel.length; j++) {
					if (origin.charAt(i) == alphabel[j]) {
						tempChar = encryptionArray[j];
					}
				}
				res += tempChar;
			}
		} else if (!encryption) {
			for (int i = 0; i < origin.length(); i++) {
				char tempChar = origin.charAt(i);
				for (int j = 0; j < encryptionArray.length; j++) {
					if (origin.charAt(i) == encryptionArray[j]) {
						tempChar = alphabel[j];
					}
				}
				res += tempChar;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		String testString = "Nguyen Ngoc Lam Bao Truong";
		String encrypted = encryptAString(testString, 3, true);
		String decrypted = encryptAString(encrypted, 3, false);
		System.out.println(encrypted);
		System.out.println(decrypted);
	}
}
