package onedimensionarray;

import java.util.Random;

public class MyArray {
	private int[] array;

	public MyArray(int size) {
		array = new int[size];
	}

	// update one value in array with index position
	public void update(int value, int index) {
		array[index] = value;
	}

	// delete one value in array with index position
	public void delete(int index) {
		array[index] = 1;
	}

	// find the number have appeared more one times.
	public String findDuplicateExistNumber(int[] source) {
		int count;
		String res = "";
		for (int i = 0; i < source.length; i++) {
			String tempString = source[i] + " --- ";
			if (!res.contains(tempString)) {
				count = 1;
				for (int j = i + 1; j < source.length; j++) {
					if (source[i] == source[j]) {
						count++;
					}
				}
				if (count > 1) {
					res += tempString + "appears " + count + " times in array." + "\n";
				}
			}
		}
		return res;
	}

	// search a number in array, if it's exist in array, this method will return
	// the index of that number, esle return -1
	public int searchWithLinear(int[] array, int target) {
		for (int i = 0; i < array.length; i++) {
			if (target == array[i]) {
				return i;
			}
		}
		return -1;
	}

	// search a number in array, if it's exist in array, this method will return
	// the index of that number, esle return -1
	public int searchWithBinary(int[] array, int target) {
		int high = array.length - 1;
		int low = 0;
		while (high <= low) {
			int mid = ((high + low) / 2);
			if (array[mid] == target) {
				return mid;
			} else if (array[mid] < target) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}

		}
		return -1;
	}

	public int searchWithBinary(int[] array, int target, int low, int high) {
		while (low <= high) {
			int mid = (low + high) / 2;
			if (array[mid] == target) {
				return mid;
			} else if (array[mid] < target) {
				return searchWithBinary(array, target, mid + 1, high);
			} else if (target < array[mid]) {
				return searchWithBinary(array, target, low, mid - 1);
			}
		}
		return -1;
	}

	// This method will swap two number with another.
	private static void swapTwoElement(int[] array, int i, int j) {
		int swap = array[i];
		array[i] = array[j];
		array[j] = swap;
	}

	// This method will sort all number in array with ascending order.
	public int[] sortAscendingBySelection(int[] array) {
		/*
		 * In selection sort, we basically traverse the array from first to array_length
		 * - 1 position and compare the element with all remain number. The current
		 * number will be swapped with the lowest number in all remain number.
		 * 
		 * 
		 * 
		 * Selection sort steps are as follows.
		 * 
		 * Current index is array[0], so that minimum number index is 0
		 *
		 * 1. Compare array[minIndex] & array[1]
		 * 
		 * 2. If array[minIndex] > array [1] minimum number index is 1.
		 * 
		 * 3. Compare array[minIndex] & array[2]
		 * 
		 * 4. If array[minIndex] > array[2] minimum number index is 2.
		 * 
		 * ...
		 * 
		 * 5. Compare array[minIndex] & array[n]
		 * 
		 * 6. if [minIndex] > array[n] minimum number index is n.
		 * 
		 * 
		 * 
		 * After first loop we will have lowest element at the minimum number index,
		 * then swap current number with the number at minimum number index.
		 *
		 * Repeat the same steps for array[1] to array[n-1]
		 */
		for (int i = 0; i < array.length; i++) {
			int minIndex = i;
			for (int j = i + 1; j < array.length; j++) {
				if (array[minIndex] > array[j]) {
					minIndex = j;
				}
			}
			swapTwoElement(array, i, minIndex);
		}
		return array;
	}

	public int[] sortAscendingByBubble2(int[] array) {
		for (int i = 1; i < array.length - 1; i++) {
			for (int j = array.length - 1; j <= i; j--) {
				if (array[j] > array[j + 1]) {
					swapTwoElement(array, j, j + 1);
				}
			}
		}
		return array;
	}

	public int[] sortAscendingByBubble(int[] array) {

		/*
		 * In bubble sort, we basically traverse the array from first to array_length -
		 * 1 position and compare the element with the next one. Element is swapped with
		 * the next element if the next element is greater.
		 *
		 *
		 *
		 * Bubble sort steps are as follows.
		 *
		 * 1. Compare array[0] & array[1]
		 * 
		 * 2. If array[0] > array [1] swap it.
		 * 
		 * 3. Compare array[1] & array[2]
		 * 
		 * 4. If array[1] > array[2] swap it.
		 * 
		 * ...
		 * 
		 * 5. Compare array[n-1] & array[n]
		 * 
		 * 6. if [n-1] > array[n] then swap it.
		 *
		 * 
		 * 
		 * After this step we will have largest element at the last index.
		 *
		 * Repeat the same steps for array[1] to array[n-1]
		 * 
		 */

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length - i - 1; j++) {
				if (array[j] > array[j + 1]) {
					swapTwoElement(array, j, j + 1);
				}
			}
		}
		return array;

	}

	public int[] sortAscendingByInsertion(int[] array) {
		int holder; // the item to be inserted
		for (int i = 1; i < array.length; i++) // Start with 1 (not 0)
		{
			holder = array[i];
			int j = i;
			while (j > 0 && array[j - 1] > holder) // Larger values are moving
													// up
			{
				array[j] = array[j - 1];
				j--;
			}
			array[j] = holder; // Put the inserted item in its proper location
		}
		return array;

	}

	/*
	 * merge sort function:
	 * 
	 * mergeSort(m)
	 * 
	 * VAR list left, right, result
	 * 
	 * if length(m) <= 1 return m
	 * 
	 * else
	 * 
	 * VAR middle = length(m) / 2
	 * 
	 * for each x in m up to middle - 1 add x to left
	 * 
	 * for each x in m at and after middle add x to right
	 * 
	 * left = mergeSort(left)
	 * 
	 * right = mergeSort(right)
	 * 
	 * if last(left) <= first(right) append right to left
	 * 
	 * return left
	 * 
	 * result = merge(left, right)
	 * 
	 * return result
	 */
	public int[] sortAscendingByMergeSort(int[] array) {
		int[] left, right, result;
		if (array.length <= 1) {
			return array;
		} else {
			// divide original array to two array.
			int midle = array.length / 2;
			left = new int[midle];
			for (int i = 0; i < left.length; i++) {
				left[i] = array[i];
			}

			right = new int[array.length - midle];
			for (int i = 0; i < right.length; i++) {
				right[i] = array[i + midle];
			}

			// call recursive mergeSort for left and right.
			left = sortAscendingByMergeSort(left);
			right = sortAscendingByMergeSort(right);
			result = merge(left, right);
			return result;

		}
	}

	private int[] merge(int[] left, int[] right) {
		int[] result = new int[left.length + right.length];
		int leftIndex = 0, rightIndex = 0;
		for (int k = 0; k < result.length; k++) {
			if (rightIndex >= right.length || (leftIndex < left.length && left[leftIndex] <= right[rightIndex])) {
				result[k] = left[leftIndex];
				leftIndex++;
			} else {
				result[k] = right[rightIndex];
				rightIndex++;
			}
		}
		return result;
	}

	public int[] sortAscendingByQuickSort(int[] array, int low, int high) {
		// check for one element or null array.
		if (array.length <= 1) {
			return array;
		}

		if (low >= high) {
			return array;
		}
		// Get the pivot element from the random index of the list
		Random random = new Random();
		int randomIndex = random.nextInt(array.length);
		int pivot = array[randomIndex];

		// Make left < pivot and pivot < right
		int leftIndex = low, rightIndex = high;
		while (leftIndex <= rightIndex) {
			// Check until all values on left side array are lower than pivot
			while (array[leftIndex] < pivot) {
				leftIndex++;
			}
			// Check until all values on right side array are greater than pivot
			while (pivot < array[rightIndex]) {
				rightIndex--;
			}
			// Now compare values from both side of lists to see if they need
			// swapping
			// After swapping move the iterator on both lists
			if (leftIndex <= rightIndex) {
				swapTwoElement(array, leftIndex, rightIndex);
				leftIndex++;
				rightIndex--;
			}

			// Do same operation as above recursively to sort two sub arrays
			if (low < rightIndex) {
				sortAscendingByQuickSort(array, low, rightIndex);
			}

			if (leftIndex < high) {
				sortAscendingByQuickSort(array, leftIndex, high);
			}
		}
		return array;
	}

	// Sort array with heap sort method. This method is not completely.
	public int[] sortAscendingByHeapSort(int[] array) {
		// create a temporary array
		int[] tempArr = new int[array.length + 1];
		tempArr[0] = -999999999; // index 0 is not used so set to -999999999
		System.arraycopy(array, 0, tempArr, 1, array.length);

		// create a sorted array to hold result
		int[] sorted = new int[array.length + 1];
		sorted[0] = -999999999; // index 0 is not used so set to -999999999

		// sort
		for (int i = tempArr.length - 1, j = 1; i >= 1; i--, j++) {
			buildHeap(tempArr, i);
			sorted[j] = tempArr[1];
			tempArr[1] = tempArr[i]; // put last element to root node
		}

		// set the result
		System.arraycopy(sorted, 1, array, 0, array.length);
		return array;
	}

	// Build a heap with the minimum number is the root node
	public void buildHeap(int[] array, int size) {
		int tempMin, left, right;

		// taking 1 as the start index
		// if parent node = i
		// so left child = 2i
		// and right child = 2i+1
		for (int i = size / 2; i >= 1; i--) {
			left = i * 2;
			right = i * 2 + 1;
			tempMin = i;

			/*
			 * if left child is lower than parent, it will be currently smallest number
			 */
			if (left <= size && array[left] < array[tempMin]) {
				tempMin = left;
			}

			/*
			 * if right child is lower than parent and left child, it will be currently
			 * smallest number
			 */
			if (right <= size && array[right] < array[tempMin]) {
				tempMin = right;
			}

			/*
			 * if parent node is not smallest number, it will be replace by smallest number
			 */
			if (tempMin != i) {
				swapTwoElement(array, i, tempMin);
				buildHeap(array, size);
			}
		}
	}

	public int[] sortAscendingByShellSort(int[] array) {
		if (array.length < 2) {
			return array;
		}
		// we start with a bigger gap
		int size = array.length;
		int gap = size / 2;
		while (gap > 0) {
			// now we will do the insertion sort of the gaped elements
			int i = gap;
			while (i < size) {
				int holder = array[i];
				int j;
				// shifting gap sorted element to correct location
				for (j = i; j >= gap && array[j - gap] > holder; j -= gap) {
					array[j] = array[j - gap];
				}
				array[j] = holder;

				// increase i
				i++;
			}
			// reduce the gap by half
			gap = gap / 2;
		}
		return array;
	}

	// Insert a new element to a place in an array.
	public static int[] insertANumberIntoArray(int[] array, int index, int number) {
		int[] result = new int[array.length + 1];
		System.arraycopy(array, 0, result, 0, array.length); // the last element
																// of result is
																// empty.
		int lastIndex = result.length - 1;
		for (int i = lastIndex - 1; i >= index; i--) {
			result[i + 1] = result[i]; // Start by shifting rightmost.
		}
		result[index] = number; // Ready to place the new element.
		return result;
	}

	private static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyArray test = new MyArray(5);
		int[] arr1 = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 25, 3, 6, 4, 55, 87, 968, 1, 3, 6548, 23, 4, 8, 245, 687 };
		int[] arr2 = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 25, 3, 6, 4, 55, 87, 968, 1, 3, 6548, 23, 4, 8, 245, 687 };
		int[] arr3 = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 25, 3, 6, 4, 55, 87, 968, 1, 3, 6548, 23, 4, 8, 245, 687 };
		int[] arr4 = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 25, 3, 6, 4, 55, 87, 968, 1, 3, 6548, 23, 4, 8, 245, 687 };
		int[] arr5 = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 25, 3, 6, 4, 55, 87, 968, 1, 3, 6548, 23, 4, 8, 245, 687 };
		int[] arr6 = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 25, 3, 6, 4, 55, 87, 968, 1, 3, 6548, 23, 4, 8, 245, 687 };
		int[] arr7 = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 25, 3, 6, 4, 55, 87, 968, 1, 3, 6548, 23, 4, 8, 245, 687 };
		int[] arr0 = { 2, 3, 4, 5, 6, 7, 8, 9, 8, 9 };
		// System.out.println(test.findDuplicateExistNumber(arr));
		System.out.println(test.searchWithLinear(arr1, 9));
		System.out.println(test.searchWithBinary(arr0, 9));
		printArray(test.sortAscendingBySelection(arr1));
		printArray(test.sortAscendingByBubble(arr2));
		printArray(test.sortAscendingByInsertion(arr3));
		printArray(test.sortAscendingByMergeSort(arr4));
		printArray(test.sortAscendingByQuickSort(arr5, 0, arr1.length - 1));
		printArray(test.sortAscendingByHeapSort(arr6));
		printArray(test.sortAscendingByShellSort(arr7));
		printArray(insertANumberIntoArray(arr1, 2, 99999));
	}
}
