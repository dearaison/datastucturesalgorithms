package exerciseofarraylist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ArrayListReader {
	public static ArrayList<String> readFile(String inputPath) {
		ArrayList<String> result = new ArrayList<>();
		try {
			File file = new File(inputPath);
			FileReader reader = new FileReader(file);
			BufferedReader in = new BufferedReader(reader);
			String line;
			while ((line = in.readLine()) != null) {
				result.add(line);
			}
			reader.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	public static void print(ArrayList<String> a) {
		for (int i = 0; i < a.size(); i++) {
			System.out.println(a.get(i));
		}
	}

	public static void readColumnOfFile(String inputPath) {
		ArrayList<String> column1 = new ArrayList<>();
		ArrayList<String> column2 = new ArrayList<>();
		ArrayList<String> column3 = new ArrayList<>();
		try {
			File file = new File(inputPath);
			FileReader reader = new FileReader(file);
			BufferedReader in = new BufferedReader(reader);
			String line;
			while ((line = in.readLine()) != null) {
				int count = 1;
				StringTokenizer tokenizer = new StringTokenizer(line, "\t");
				while (tokenizer.hasMoreTokens()) {
					if (count == 1) {
						column1.add(tokenizer.nextToken());
					}
					if (count == 2) {
						column2.add(tokenizer.nextToken());
					}
					if (count == 3) {
						column3.add(tokenizer.nextToken());
					}
					if (count < 4) {
						count++;
					}
				}
			}
			reader.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		print(column1);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		print(readFile("B:\\baotr\\Documents\\Data structure and algorithsm\\Exercise\\BaiTap 2_L.O.V.E.txt"));
		readColumnOfFile("B:\\baotr\\Documents\\Data structure and algorithsm\\Exercise\\a.txt");	
	}

}
