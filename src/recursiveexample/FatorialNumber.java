package recursiveexample;

public class FatorialNumber {
	public int toCountAFatorialNumber(int n){		
		if (n == 0) {
			return 1;
		} else if (n > 1) {
			return n * toCountAFatorialNumber(n-1);
		}
		return 1;
	}
	public static void main(String[] args) {
		FatorialNumber test1 = new FatorialNumber();
		System.out.println(test1.toCountAFatorialNumber(30));
	}
}
