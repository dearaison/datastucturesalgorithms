package recursiveexample;

public class TokyoTowerGameSolving {
	public void solve(int diskNumber, char from, char inter, char to) {
		if (diskNumber == 1) {
			System.out.println("Disk " + diskNumber + " from " + from + " to " + to);
		} else {
			solve(diskNumber - 1, from, to, inter);
			System.out.println("Disk " + diskNumber + " from " + from + " to " + to);
			solve(diskNumber - 1, inter, from, to);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TokyoTowerGameSolving test1 = new TokyoTowerGameSolving();
		int theNumberOfDisk = 12;
		test1.solve(theNumberOfDisk, 'A', 'B', 'C');
	}

}
