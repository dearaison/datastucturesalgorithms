package recursiveexample;

public class DecimalToBinary {

	public String convertANumberFromDecToBin(int number) {
		if (number == 0) {
			return "0";
		} else if (number == 1) {
			return "1";
		} else if (number > 1) {
			int remain = number % 2;
			int even = number / 2;
			return convertANumberFromDecToBin(even) + "" + remain;
		}
		return null;
	}

	public static void main(String[] args) {
		DecimalToBinary test1 = new DecimalToBinary();
		System.out.println(test1.convertANumberFromDecToBin(7));
	}
}
