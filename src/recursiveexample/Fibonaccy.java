package recursiveexample;

public class Fibonaccy {

	public int computeFibonaccyNumber(int times) {
		/*
		 * F(n) = 1 when n = 1;
		 * 
		 * F(n) = 1 when n = 2;
		 * 
		 * F(n) = F(n-1) + F(n-2) when n > 2.
		 */

		if (times == 1) {
			return 1;
		} else if (times == 2) {
			return 1;
		} else if (times > 2) {
			return computeFibonaccyNumber(times - 1) + computeFibonaccyNumber(times - 2);
		}
		return 0;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Fibonaccy test1 = new Fibonaccy();
		System.out.println(test1.computeFibonaccyNumber(6));
	}

}
